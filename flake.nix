{
  description = "samuelsung's build of st";

  inputs.commitlint-config.url = "git+https://codeberg.org/samuelsung/commitlint-config";
  inputs.devenv.url = "github:cachix/devenv";
  inputs.flake-parts.url = "github:hercules-ci/flake-parts";
  inputs.mk-shell-bin.url = "github:rrbutani/nix-mk-shell-bin";
  inputs.nixpkgs.url = "nixpkgs/nixos-unstable";
  inputs.nix2container.url = "github:nlewo/nix2container";

  outputs = inputs@{ flake-parts, ... }:
    flake-parts.lib.mkFlake { inherit inputs; }
      {
        imports = [
          flake-parts.flakeModules.easyOverlay
          inputs.devenv.flakeModule
        ];
        systems = [ "x86_64-linux" ];
        perSystem = { config, system, pkgs, ... }:
          {
            packages = {
              st-samuelsung = pkgs.st.overrideAttrs (_: {
                version = "master";
                src = ./.;
                buildInputs = with pkgs; [
                  xorg.libX11
                  xorg.libXft
                  xorg.libXcursor
                  harfbuzz
                ];
              });
            };

            overlayAttrs = {
              inherit (config.packages)
                st-samuelsung;
            };

            checks = {
              inherit (config.packages)
                st-samuelsung;
            };

            devenv.shells.default = {
              pre-commit.hooks = {
                deadnix.enable = true;
                nixpkgs-fmt.enable = true;
                statix.enable = true;

                commitlint = inputs.commitlint-config.hook.${system};
              };
            };
          };
      };
}
