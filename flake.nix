{
  description = "samuelsung's build of st";

  inputs.flake-utils.url = "github:numtide/flake-utils";

  outputs = { self, nixpkgs, flake-utils }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        name = "st";
        pkgs = nixpkgs.legacyPackages.${system};
        package = pkgs.st.overrideAttrs (_: {
          version = "master";
          src = ./.;
          buildInputs = with pkgs; [ xorg.libX11 xorg.libXft xorg.libXcursor harfbuzz ];
        });
      in
      rec {
        apps = { ${name} = { type = "app"; program = "${defaultPackage}/bin/${name}"; }; };
        defaultPackage = package;
        packages.${name} = defaultPackage;
        defaultApp = apps.${name};
      }
    );
}
